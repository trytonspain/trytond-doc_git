=====================
Facturación analítica
=====================

Añade una cuenta analítica en las líneas de factura y genera líneas analíticas por los asientos de la factura.

Dependencias
------------

* `Contabilidad analítica`_
* Facturación_

.. _Contabilidad analítica: ../analytic_account/index.html
.. _Facturación: ../account_invoice/index.html
