===========================
Iniciativas y oportunidades
===========================

* Iniciativas comerciales
* Conversión de iniciativas a oportunidades
* Gestión de las conversiones
* Registro de iniciativas perdidas
* Oportunidades a pedidos de venta


.. inheritref:: sale_opportunity/sale_opportunity:section:iniciativas

Iniciativas
===========

TODO

.. inheritref:: sale_opportunity/sale_opportunity:section:oportunidades

Oportunidades
=============

TODO
