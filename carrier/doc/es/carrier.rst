==============
Transportistas
==============

.. inheritref:: carrier/carrier:section:transportistas

Transportistas
==============

Los transportistas van relacionados con un tercero y con un producto.

* Abra el menú |menu_carrier| i cree un nuevo transportista.
* Seleccione el |party| y |carrier_product|.
* Seleccione el |carrier_cost_method|.

Los |carrier_cost_method| disponibles son:

.. inheritref:: carrier/carrier:bullet_list:carrier_cost_method

* Producto


.. |menu_carrier| tryref:: carrier.menu_carrier/complete_name
.. |party| field:: carrier/party
.. |carrier_product| field:: carrier/carrier_product
.. |carrier_cost_method| field:: carrier/carrier_cost_method
