===========================
Planificador del calendario
===========================

Notificación de asistencia de evento entre distintos usuarios.

El correo electrónico usa el servidor de SMTP definido en el servidor de Tryton.

Dependencias
------------

* Calendario_

.. _Calendario: ../calendar/index.html
