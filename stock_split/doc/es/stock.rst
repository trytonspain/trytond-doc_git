#:after:stock/stock:section:movimientos#

-----------------
Dividir albaranes
-----------------

A las líneas de albarán dispone de un asistente que permite dividir las lineas del
albarán. Debe especificar las cantidad a dividir y ese albarán se dividirá en dos.

Si se rellena el campo *Contador*, sólo se dividirá ese número de veces. Puede
crearse una línea con la cantidad restante.
