================
Compra analítica
================

Añade contabilidad analítica a las líneas de los pedidos de compra.

Dependencias
------------

* `Contabilidad analítica`_
* `Facturación analítica`_
* Compras_

.. _Contabilidad analítica: ../analytic_account/index.html
.. _Facturación analítica: ../analytic_invoice/index.html
.. _Compras: ../purchase/index.html
