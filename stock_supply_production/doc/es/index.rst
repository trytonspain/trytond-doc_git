========================
Suministro de producción
========================

Añade mecanismos automáticos de suministro a través de solicitudes de
producción.

Dependencias
------------

* Producto_
* Producción_
* Stock_
* `Gestión del abastecimiento de stock`_

.. _Producto: ../product/index.html
.. _Producción: ../production/index.html
.. _Stock: ../stock/index.html
.. _Gestión del abastecimiento de stock: ../stock_supply/index.html
 
