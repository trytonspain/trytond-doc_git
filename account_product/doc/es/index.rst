=========================
Contabilidad de productos
=========================

Añade propiedades de contabilidad a productos y categorías como:

* Cuentas para gastos e ingresos
* Impuestos para cliente y proveedor

Dependencias
------------

* Contabilidad_
* Empresa_
* Producto_

.. _Contabilidad: ../account/index.html
.. _Empresa: ../company/index.html
.. _Producto: ../product/index.html
