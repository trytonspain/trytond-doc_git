#! /usr/bin/env python

from path import path

import moin_to_rst


def create_trytoncfg_file(core_module_name, dest_path):
    return write_file("""[tryton]
version=2.7.0
depends:
   %s
xml:
""" % core_module_name, dest_path)


def create_copyright_file(dest_path):
    copyright_file = path('%s/trytond_doc/COPYRIGHT' % dest_path.parent.parent)
    copyright_content = open(copyright_file).read()
    return write_file(copyright_content, dest_path)


def create_install_file(module_doc_name, dest_path):
    install_content = """Installing %s
===========%s

Prerequisites
-------------

 * Python 2.6 or later (http://www.python.org/)
 * trytond (http://www.tryton.org/)

Installation
------------

As it is not a real Tryton module, it's not required neither recommended to
install this package with setup tools.

To use without installation, extract the archive into ``trytond/modules`` with
the directory name %s.

You need to have all the dependencies (recursively) of module, included the
trytond_doc which has the manual root and Makefile to generate the manual.


""" % (module_doc_name, "=" * len(module_doc_name), module_doc_name)
    return write_file(install_content, dest_path)


def create_license_file(dest_path):
    license_file = path('%s/trytond_doc/LICENSE' % dest_path.parent.parent)
    license_content = open(license_file).read()
    return write_file(license_content, dest_path)


def create_manifest_file(doc_files, dest_path):
    doc_files = ["include %s" % dest_path.parent.relpathto(f)
            for f in doc_files]
    manifest_content = """include INSTALL
include README
include COPYRIGHT
include LICENSE
include tryton.cfg
%s
""" % "\n".join(doc_files)
    return write_file(manifest_content, dest_path)


def create_readme_file(module_doc_name, core_module_name, dest_path):
    readme_content = """
%s
%s

The %s fake module of the Tryton application platform.

It's not a real module (it doesn't have Python nor XML files). It only supply
files in 'doc' directory with the User and Administrator manual of
%s core module.

Installing
----------

See INSTALL

Support
-------

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  http://bugs.tryton.org/
  http://groups.tryton.org/
  http://wiki.tryton.org/
  irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT


For more information please visit the Tryton web site:

  http://www.tryton.org/
""" % (module_doc_name, "=" * len(module_doc_name), module_doc_name,
            core_module_name)
    return write_file(readme_content, dest_path)


def convert_page(page_path, dest_path):
    page_file = open(page_path)
    page_content = page_file.read()
    rst_content = moin_to_rst.convert_format(page_content)
    return write_file(rst_content, dest_path)


def create_modulesrst_file(module_doc_name, title, dest_path):
    return write_file("""#:before:trytond_doc/module:comment:module_list#

1. %s_

.. _%s: ../%s/index.html

""" % (' ' in title and '`%s`' % title or title, title, module_doc_name),
            dest_path)


def write_file(content, dest_path, overwrite=True):
    if dest_path.exists():
        print "%s file already exists: %s" % (dest_path.basename(), dest_path)
        if not overwrite:
            return False
        print "It is overwritted"
    dest_file = open(dest_path, 'w')
    dest_file.write(content)
    dest_file.close()
    print "writted file %s" % dest_path
    return True


core_cfg = open('../../tryton-buildout/core.cfg')
core_lines = core_cfg.readlines()
core_cfg.close()

print "discarted lines:\n%s\n" % "\n".join(core_lines[0:4])
core_lines = core_lines[4:]
core_modules = [l.split(' ')[0] for l in core_lines]
print "%s core modules" % len(core_modules)

converted_pages = []
for core_name in core_modules:
    page_title = " ".join([a.title() for a in core_name.split('_')])
    page_name = page_title.replace(' ', '')
    print "page_name of module '%s': '%s'" % (core_name, page_name)
    doc_dir = path('%s_doc' % core_name).abspath()
    if not doc_dir.exists():
        doc_dir.mkdir()
        print "path for module created: '%s'" % doc_dir
    module_doc_name = doc_dir.basename()
    # MODULE_doc/tryton.cfg
    create_trytoncfg_file(core_name, doc_dir.joinpath('tryton.cfg'))
    # MODULE_doc/COPYRIGHT
    create_copyright_file(doc_dir.joinpath("COPYRIGHT"))
    # MODULE_doc/INSTALL
    create_install_file(module_doc_name, doc_dir.joinpath("INSTALL"))
    # MODULE_doc/LICENSE
    create_license_file(doc_dir.joinpath("LICENSE"))
    # MODULE_doc/README
    create_readme_file(module_doc_name, core_name, doc_dir.joinpath('README'))
    # MODULE_doc/doc
    doc_doc_dir = doc_dir.joinpath('doc')
    if not doc_doc_dir.exists():
        doc_doc_dir.mkdir()
        print "path for doc files created: '%s'" % doc_doc_dir
    # MODULE_doc/doc/es
    doc_doc_es_dir = doc_doc_dir.joinpath('es')
    if not doc_doc_es_dir.exists():
        doc_doc_es_dir.mkdir()
        print "path for Spanish doc files created: '%s'" % doc_doc_es_dir
    doc_files = []
    page_file = path('pages_contents/%s.rst' % page_name)
    if not page_file.exists():
        # MODULE_doc/doc/TODO
        print "page not found"
        todo_file = doc_doc_es_dir.joinpath('TODO')
        if not todo_file.exists():
            todo_file.touch()
            print "TODO file created: '%s'" % todo_file
        print ""
        doc_files.append(todo_file)
    else:
        # MODULE_doc/doc/index.rst
        index_file = doc_doc_es_dir.joinpath('index.rst')
        convert_page(page_file, index_file)
        doc_files.append(index_file)
        # MODULE_doc/doc/modules.rst
        #modules_file = doc_doc_es_dir.joinpath("modules.rst")
        #create_modulesrst_file(module_doc_name, page_title, modules_file)
        #doc_files.append(modules_file)
        converted_pages.append(page_file)
    # MODULE_doc/MANIFEST.in
    create_manifest_file(doc_files, doc_dir.joinpath("MANIFEST.in"))
    print "doc module %s created successful" % doc_dir
print ""

for page_file in path('pages_contents').files('*.rst'):
    if page_file in converted_pages:
        continue
    todo_file = path('page_todo/%s' % page_file.basename())
    path.copy(page_file, todo_file)
    print "copied Page file into TODO: %s" % todo_file
print "END"

