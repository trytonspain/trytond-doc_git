====================
Medidas de productos
====================

Añade medidas al producto (longitud, alto, ancho, peso).

Dependencias
------------

* Producto_

.. _Producto: ../product/index.html
