========
Proyecto
========

El módulo **proyecto** le permite la gestión de proyectos y tareas.

* **Proyectos** podrá dar de alta los proyectos que está
  trabajando su empresa.
* **Tareas** podrá dar de alta los tareas asociados a proyectos
  o a otras tareas (árbol).

Dependencias
------------

* `Tiempo por compañía`_
* `Tiempos de trabajo`_
* Empresa_

.. _Tiempo por compañía: ../company_work_time/index.html
.. _Tiempos de trabajo: ../timesheet/index.html
.. _Empresa: ../party/index.html
