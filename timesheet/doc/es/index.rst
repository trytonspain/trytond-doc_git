=================
Partes de trabajo
=================

Módulo de partes de trabajo con:

* Trabajos o tareas
* Líneas de parte de trabajo

Y con informes:

* Horas por trabajo
* Horas por empleado y semana
* Horas por empleado y mes

Dependencias
------------

* Stock_
* Empresa_
* `Tiempo de trabajo en la empresa`_

.. _Stock: ../stock/index.html
.. _Empresa: ../company/index.html
.. _Tiempo de trabajo en la empresa: ../company_work_time/index.html
