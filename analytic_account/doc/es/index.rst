======================
Contabilidad analítica
======================

Módulo financiero y contable con:

* Contabilidad analítica con planes analíticos ilimitados

Y con informes:

* Balance contable analítico

Dependencias
------------

* Contabilidad_
* Empresa_
* Moneda_
* Terceros_

.. _Contabilidad: ../account/index.html
.. _Empresa: ../company/index.html
.. _Moneda: ../currency/index.html
.. _Terceros: ../party/index.html
