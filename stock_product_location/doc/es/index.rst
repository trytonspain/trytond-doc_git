===============================
Ubicación de stock de producto
===============================

Define la ubicación interna predeterminada por almacén y producto.

Estas ubicaciones se utilizarán en el envío de proveedor para generar movimientos
de inventario.

Dependencias
------------

* Stock_
* Product_

.. _Stock: ../stock/index.html
.. _Product: ../product/index.html
