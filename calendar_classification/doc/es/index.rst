============================
Clasificación del calendario
============================

Añade en la gestión de calendario:

* Filtrado por la búsqueda de eventos (propietario, classificación...)
* Permisos de lectura, escritura i eliminación de eventos.

Dependencias
------------

* Calendario_

.. _Calendario: ../calendar/index.html
