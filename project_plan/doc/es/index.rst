==========================
Planificación de proyectos
==========================

El módulo **Planificación de proyectos** le permite la planificación de proyectos
y tareas. A cada tarea o proyecto le podrá indicar una fecha de inicio y fin.

También puede anotar a parte del día previsto de la tarea, el día real que realiza
la tarea, con el que obtendrá, el retardo de dias de la tarea.

Dependencias
------------

* Empresa_
* Proyecto_
* `Tiempo de trabajo`_

.. _Empresa: ../company/index.html
.. _Proyecto: ../project/index.html
.. _Tiempo de trabajo: ../timesheet/index.html
