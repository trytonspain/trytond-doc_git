==============================================
Pedido de compras a partir de líneas de compra
==============================================

Modifica el comportamiento de los pedidos de compra permitiendo crear líneas de
factura independientes en lugar de una factura completa. Esto permite crear
facturas con líneas que procedan de distintas compras.

Dependencias
------------

* Compras_
* `Facturación de líneas de factura`_

.. _Compras: ../purchase/index.html
.. _Facturación de líneas de factura: ../account_invoice_line_standalone/index.html
