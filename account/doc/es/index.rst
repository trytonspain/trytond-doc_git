============
Contabilidad
============

Módulo financiero y contable con:

* Contabilidad general
* Gestión de ejercicios fiscales
* Gestión de impuestos
* Libros diarios
* Conciliación

Y con los informes:

* Libro mayor
* Balance de sumas y saldos
* Balance de situación
* Balance pérdidas y ganancias
* Balance de terceros
* Saldo vencido
* Libro diario

Dependencias
------------

* Empresa_
* Moneda_
* Terceros_

.. _Empresa: ../company/index.html
.. _Moneda: ../currency/index.html
.. _Terceros: ../party/index.html
