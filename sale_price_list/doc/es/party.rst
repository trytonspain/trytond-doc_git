#:before:party/party:section:informes#

Tarifa
------

En el **tercero** le podemos seleccionar que tarifa de venta se
usará en las pestaña **Tarifa**. Cuando seleccione un tercero en el pedido de
venta, se le asignará automáticamente la tarifa de venta del cliente. Después
si lo desea, puede cambiar por otra tarifa.
