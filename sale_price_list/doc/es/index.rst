================
Tarifas de venta
================

Permite definir la tarifa por tercero y pedido de venta.

Dependencias
------------

* Tercero_
* Producto_
* `Tarifas de producto`_
* Ventas_

.. _Tercero: ../party/index.html
.. _Producto: ../product/index.html
.. _Tarifas de producto: ../product_price_list/index.html
.. _Ventas: ../sale/index.html

