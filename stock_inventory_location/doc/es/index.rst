===================================
Inventario de stock por ubicaciones
===================================

Añade un asistente que permite crear inventarios automáticos para una lista
dada de ubicaciones.

Dependencias
------------

* Stock_
* Product_
* Empresa_

.. _Stock: ../stock/index.html
.. _Product: ../product/index.html
.. _Empresa: ../company/index.html
