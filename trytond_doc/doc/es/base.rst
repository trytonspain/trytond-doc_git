===========================
Conceptos básicos de Tryton
===========================

En el diseño de Tryton se ha tenido muy en cuenta la ergonomía de su uso. A
diferencia de otras aplicaciones, está pensada por y para facilitar la operativa
diaria al usuario. Su sencillez y extremada flexibilidad permite que cualquier
persona no iniciada en el uso de aplicaciones informáticas pueda aprender su
manejo en un tiempo extremadamente corto. La curva de aprendizaje no resulta
costosa y en unos pocos días se puede estar utilizando la aplicación al 100%
obteniendo el máximo provecho de su uso.

.. toctree::
   :maxdepth: 1

   tryton_conexion
   tryton_toolbar
   tryton_menus
   tryton_campos
   tryton_idioma
   tryton_acciones
   tryton_informes
   tryton_bookmarks
   tryton_buscador
   tryton_adjuntos
   tryton_import_export
   tryton_request
   tryton_trucos
