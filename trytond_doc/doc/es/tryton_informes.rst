========
Informes
========

Tryton dispone en muchos objetos de un informe general: **Imprimir pantalla**.
Este informe imprime los registros y campos disponibles en la vista de lista.

Además, muchos de los principales objetos, disponen de informes por defecto:

* Venta
* Factura
* Albarán no valorado
* ...

-----------------
Acceso a informes
-----------------

Tryton dispone de tres botones para trabajar con informes. 

.. figure:: images/tryton-print.png

   Imprimir registros desde Tryton


Para trabajar con informes hay que:

* Seleccionar uno o varios registros.
* Hacer clic sobre el icono **Herramientas** y seleccionar el menú de impresión
  que se desee o hacer clic directamente sobre el icono de impresión adecuado.
* Escoger el tipo de informe que se desee imprimir.

Abrir informe
-------------

Tryton muestra el informe en el procesador de textos que se tenga definido por defecto
(Libre Office, Open Office, etc.) y permite su edición.

Informe por email
-----------------

Tryton abre el gestor de correos electrónicos que se tenga definido por defecto
y adjunta el informe para su envío.

Imprimir informe
----------------

Tryton muestra el informe en el procesador de textos que se tenga definido por
defecto (Libre Office, Open Office, etc.) en modo de sólo lectura. Si se dispone
de Jasper Reports, se pueden diseñar informes persoanilzados y salida en PDF.

--------------------
Personalizar informe
--------------------

Para personalizar los informes de la empresa hay que abrir la ficha de la misma:
menú **Terceros/Configuración/Empresas** y en la pestaña **Informes** poner la
cabecera del informe y/o el pie de página que se desee (requiere conocimientos
técnicos mínimos de RML).

Otro tipo de personalizaciones, que se deberían hacer accediendo a los ficheros
del servidor o instalando nuevos módulos, se escapan del alcance de este manual.
