==================
Acciones en Tryton
==================

Una *acción* en Tryton es un asistente que realizará cálculos para generar unos
valores nuevos o nuevos registros. Generalmente una acción abre una ventana
flotante, que contendrá un formulario, para completar y procesar dicha información.

--------------------
Desde la vista lista
--------------------

Para ejecutar una acción desde la vista de lista hay que:

 * Seleccionar los registros de la lista sobre los que queremos ejecutar la acción.
   Para ello es suficiente hacer clic con el ratón en el primero de los registros,
   pulsar la tecla **Mayúsculas** y sin soltarla, marcar el último de los registros
   que deseamos incluir en la selección. Si los registros no están consecutivos,
   si no que deseamos seleccionar registros "salteados", en lugar de pulsar la
   tecla **Mayúsculas**, pulsaremos la tecla *Control* e iremos marcando
   uno por uno los registros que queramos incluir en la selección.

 * Hacer clic en sobre icono **Ejecutar acción** y seleccionar la acción que se
   desee ejecutar.

-------------------------
Desde la vista formulario
-------------------------

Desde la vista de formulario, sólo se puede ejecutar acciones para el registro
que esté abierto en ese momento, y el procedimiento es el mismo que desde la
vista de lista.

.. figure:: images/tryton-action.png

   Acción desde un pedido de venta para el envío de correo electrónico en Tryton
