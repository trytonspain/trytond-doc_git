=================
Usuarios / Grupos
=================

.. tip:: Una empresa tiene X trabajadores, pero no todos los trabajadores
         disponen de acceso a nuestro ERP. Por tanto, sólo deberemos dar de alta
         usuarios que dispondrá acceso a nuestro ERP.

------
Grupos
------

* **Administración/Usuarios/Grupos**. Grupos que pertenecerán a nuestros usuarios

Al instalar los módulos estaran disponibles ciertos grupos por defecto. Estos
grupos darán acceso a ciertos objetos, como si los usuarios sólo pueden consultar
o editar los registros del objecto.

--------
Usuarios
--------

* **Administración/Usuarios/Usuarios**. Usuarios en nuestro sistema

Añade la información del usuario y grupos.

.. warning:: Si añade un usuario un nuevo grupo, este usuario deberá identificarse
             de nuevo al ERP para que se activen los nuevos grupos que se le han
             asignado.

.. note:: No elimine usuarios. Si un usuario ya no trabaja o está vacante, desactive
          su acceso del sistema.

Los usuarios pueden editar su firma, idioma, contraseña mediante el menú principal
**Usuarios**.
