=====
Menús
=====

Una vez realice el acceso/login al ERP, se le mostrará un menú principal de todas
las opciones que dispone acceso (según usuario, grupo y permisos). Mediante este
menú puede acceder a las distintas secciones de su ERP.

Menú favoritos
==============

Al lado de cada menú dispone de una estrella. Si la marca convertirá este menú
como menú favorito. 
Accediendo al menú superior **Favoritos** dispone de todos los menús que ha marcado.
Esto es útil para disponer de aquellos menús que usa con frecuencia el usuario.

.. figure:: images/tryton-bookmarks.png

   Marcando y accediendo a los menús favoritos
