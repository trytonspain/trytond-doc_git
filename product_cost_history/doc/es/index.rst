=========================================
Historial del precio de costo de producto
=========================================

Genera un histórico del precio de costo de producto.

En el producto dispone de un listado de todos los valores pasados del precio de
coste del producto actual.

Dependencias
------------

* Producto_

.. _Producto: ../product/index.html
