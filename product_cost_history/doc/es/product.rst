#:before:product/product:section:unidades_de_medida#

Historial del precio del coste
------------------------------

En la variante del producto (|menu_product|) dispone de un nuevo elemento
relacionado: Historial del coste.

Al seleccionar esta opción, dispondremos de una nueva venta con todo el historial
de la variante (producto) con la fecha y el precio de coste de esta fecha.

.. |menu_product| tryref:: product.menu_product/complete_name
