=============================
Coste de envío en las compras
=============================

Añade coste de envío en las compras

Dependencias
------------

* Compras_
* Transportista_

.. _Compras: ../purchase/index.html
.. _Transportista: ../carrier/index.html
