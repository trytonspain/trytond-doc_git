======
Ventas
======

Gestión de pedidos de venta y albaranes y facturación.

Dependencias
------------

* Empresa_
* Tercero_
* Moneda_
* Producto_
* Contabilidad_
* Facturación_
* `Contabilidad de productos`_
* Stock_

.. _Empresa: ../company/index.html
.. _Tercero: ../party/index.html
.. _Moneda: ../currency/index.html
.. _Producto: ../product/index.html
.. _Contabilidad: ../account/index.html
.. _Facturación: ../account_invoice/index.html
.. _Contabilidad de productos: ../account_product/index.html
.. _Stock: ../stock/index.html


