=====================
Ingresos por proyecto
=====================

Añade productos y costes/ingresos a proyectos o tareas.

En los empleados, deberemos añadir el **coste** de este.

Dependencias
------------

* Proyecto_
* `Tiempos de trabajo`_
* Empresa_
* Producto_

.. _Proyecto: ../project/index.html
.. _Tiempos de trabajo: ../timesheet/index.html
.. _Empresa: ../company/index.html
.. _Producto: ../product/index.html
