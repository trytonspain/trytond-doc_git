=======================
Abastecimiento de stock
=======================

Gestión de abastecimientos con:

* Reglas de stock mínimo
* Solicitudes de compra

Con planificadores para:

* Generar solicitudes de compra basadas en stocks mínimos
* Generar envíos internos basados en stocks mínimos

Dependencias
------------

* Empresa_
* Tercero_
* Stock_
* Product_
* Compras_
* Contabilidad_

.. _Empresa: ../company/index.html
.. _Tercero: ../party/index.html
.. _Stock: ../stock/index.html
.. _Product: ../product/index.html
.. _Compras: ../purchase/index.html
.. _Contabilidad: ../account/index.html
