===================
Tarifas de producto
===================

Define reglas de tarifa por tercero.

Dependencias
------------

* Party_
* Company_
* Product_

.. _Party: ../party/index.html
.. _Company: ../company/index.html
.. _Product: ../product/index.html
