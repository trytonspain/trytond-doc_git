======================================
Facturar a partir de líneas de factura
======================================

Permite crear una factura manualmente y en ella seleccionar líneas de factura ya
creadas y añadirlas en la nueva factura que está creando.

Sólo podrá seleccionar las líneas de factura del tercero de la factura y que no sean
facturadas previamente.

Dependencias
------------

* Contabilidad_
* Facturación_

.. _Contabilidad: ../account/index.html
.. _Facturación: ../account_invoice/index.html
