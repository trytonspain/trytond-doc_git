======================
Coste FIFO de producto
======================

Añade el método de coste FIFO en el formulario de producto.

Una vez instalado, el precio de los movimientos de stock desde un proveedor o a
un cliente actualizará automáticamente el precio de coste del producto relacionado
(si el método de cálculo del precio de coste es FIFO).

Dependencias
------------

* Producto_
* Stock_

.. _Producto: ../product/index.html
.. _Stock: ../stock/index.html
