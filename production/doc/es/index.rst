==========
Producción
==========

Define los conceptos básicos para la gestión de la producción: lista de
materiales y orden de producción.

Dependencias
------------

* Empresa_
* Producto_
* Stock_

.. _Empresa: ../company/index.html
.. _Producto: ../product/index.html
.. _Stock: ../stock/index.html
 
