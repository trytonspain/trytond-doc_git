=======
Compras
=======

Gestión de pedidos de compra con la posibilidad de facturación y envío del pedido
de compra.

Dependencias
------------

* Empresa_
* Tercero_
* Stock_
* Contabilidad_
* Facturación_
* `Contabilidad de productos`_
* Moneda_

.. _Empresa: ../company/index.html
.. _Tercero: ../party/index.html
.. _Stock: ../stock/index.html
.. _Contabilidad: ../account/index.html
.. _Facturación: ../account_invoice/index.html
.. _Contabilidad de productos: ../account_product/index.html
.. _Moneda: ../currency/index.html
