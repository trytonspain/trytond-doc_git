========
Empresas
========

.. inheritref:: company/company:section:gestion_de_empresas

Empresas
--------

La empresa es la nuestra compañía.

* Abra el menú |menu_company_list| i cree un nueva empresa.
* Seleccione el |party| y |currency|.

.. |menu_company_list| tryref:: company.menu_company_list/complete_name
.. |party| field:: company.company/party
.. |currency| field:: company.company/currency

.. inheritref:: company/company:section:usuarios

Usuarios
--------

Una vez creadas la empresa es importante que los usuarios del sistema añadir las
empresas que podrá operar ese usuario.

Cada usuario, des del apartado de sus preferencies, podrá decidir que empresa opera
en este momento. A la parte inferior izquierda del cliente de escritorio podrá ver
el nombre de la empresa que está en ese momento.
