============
Facturación
============

Módulo financiero y contable con:

* Plazos de pago
* Factura / Abono
* Factura de proveedor / Abono de proveedor

Con las posibilidades de:

* Hacer el seguimiento del pago de facturas.
* Definir secuencias de facturas por ejercicio fiscal o período.
* Abonar cualquier factura.

Dependencias
------------

* Contabilidad_
* `Contabilidad producto`_
* Empresa_
* Moneda_
* Terceros_
* Producto_

.. _Contabilidad: ../account/index.html
.. _Contabilidad producto: ../account_product/index.html
.. _Empresa: ../company/index.html
.. _Moneda: ../currency/index.html
.. _Terceros: ../party/index.html
.. _Producto: ../product/index.html
