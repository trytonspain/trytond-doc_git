===================
Extractos contables
===================

Módulo financiero y contable con:

* Extractos contables
* Diario de extracto

Dependencias
------------

* Contabilidad_
* Empresa_
* Producto_
* Facturación_
* Moneda_

.. _Contabilidad: ../account/index.html
.. _Empresa: ../company/index.html
.. _Producto: ../product/index.html
.. _Facturación: ../account_invoice/index.html
.. _Moneda: ../currency/index.html
