====================================
Previsión de abastecimiento de stock
====================================

Toma en cuenta las previsiones a la hora de generar solicitudes de compra.

Dependencias
------------

* Stock_
* `Gestión del abastecimiento de stock`_
* `Previsión de abastecimiento de stock`_

.. _Stock: ../stock/index.html
.. _Gestión del abastecimiento de stock: ../stock_supply/index.html
.. _Previsión de abastecimiento de stock: ../stock_forecast/index.html
