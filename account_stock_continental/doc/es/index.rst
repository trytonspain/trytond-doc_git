=============================
Gestión de stock continental
=============================

Añade contabilidad de stocks continental para la valoración de existencias en tiempo real.

Dependencias
------------

* Contabilidad_

.. _Contabilidad: ../account/index.html
