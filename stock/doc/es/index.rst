=====
Stock
=====

Gestión de stocks y control de inventarios con:

* Definición de ubicaciones
* Movimientos de stock
* Albaranes de proveedores, clientes e internos. Albaranes de devolución de
  clientes y proveedores.
* Inventario de stock

Y con los informes:

* Albarán de entrega
* Albarán interno o de picking
* Lista de recálculo de stocks (en recepciones de proveedores y devoluciones
  de clientes)
* Productos por ubicación

Dependencias
------------

* Empresa_
* Tercero_
* Producto_
* Moneda_

.. _Empresa: ../company/index.html
.. _Tercero: ../party/index.html
.. _Producto: ../product/index.html
.. _Moneda: ../currency/index.html
