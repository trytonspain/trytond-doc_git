========================
Historial de facturación
========================

Añade un historial para los campos de la factura.

Dependencias
------------

* Contabilidad_
* Facturación_

.. _Contabilidad: ../account/index.html
.. _Facturación: ../account_invoice/index.html
