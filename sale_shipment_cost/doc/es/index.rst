======================
Ventas. Coste de envío
======================

Añade el coste de envío a las ventas.

Dependencias
------------

* `Facturación`_
* Transportista_
* Ventas_
* Stock_

.. _Facturación: ../account_invoice/index.html
.. _Transportista: ../carrier/index.html
.. _Ventas: ../sale/index.html
.. _Stock: ../stock/index.html
