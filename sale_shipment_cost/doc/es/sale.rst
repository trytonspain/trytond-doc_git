#:inside:sale/sale:bullet_list:sale_fields#

* |carrier|: Transportista para la entrega
* |shipment_cost_method|: Método de coste de envío

.. |carrier| field:: sale.sale/carrier
.. |shipment_cost_method| field:: sale.sale/shipment_cost_method
